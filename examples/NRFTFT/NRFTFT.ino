#include <SPI.h>  
#include <TFT.h>
#include "RF24.h"
#include <Wire.h>
#include "PCF8574.h"

int direction = 0;
int lastNum;

TFT tft = TFT(10, 9, 8); //CS, DC, RST - RST = 12 for v1 or 8 for v3+ 
RF24 myRadio (6, 7); //set to 2 & 3 for v2 or 6 & 7 or A1 & A2 for v3
PCF8574 pcf(0x20);
byte addresses[][6] = {"0"};

struct package {
  int id = 2;
  int num = 0;
  //float temperature = 18.3;
  //char  text[300] = "Text to be transmit";
};


typedef struct package Package;
Package dataRecieve;
Package dataTransmit;

void setup() {
  Serial.begin(115200);
  pcf.begin();
  tft.begin();
  analogWrite(5, 180); // backlight
  tft.fillScreen(BG);
  tft.setRotation(3);
  tft.setTextColor(TXT, BG);
  tft.setCursor(12, 0);
  tft.print("NRF DEMO");
  delay(500);
  
  myRadio.begin();  
  myRadio.setChannel(115); ;
  myRadio.setPALevel(RF24_PA_MAX);
  myRadio.setDataRate( RF24_1MBPS );
  
  myRadio.openReadingPipe(1, addresses[0]);
  myRadio.startListening();
}

void loop() {
  if ( myRadio.available()) {
    while (myRadio.available()){
      myRadio.read( &dataRecieve, sizeof(dataRecieve) );
    }
    Serial.println("Recieve: ");
    Serial.print("Package:");
    Serial.print(dataRecieve.num);
    Serial.print("\n");
    //Serial.println(dataRecieve.temperature);
    //Serial.println(dataRecieve.text);
    Serial.print("\n");
    tft.setCursor(20, 20);
    tft.print(dataRecieve.num);
  }

  delay(50);

  if (dataRecieve.num != lastNum) {
    tft.setCursor(20, 40);
    switch (dataRecieve.num) {
      case 0:
        tft.print("RSet     ");
        break;
      case 1:
        tft.print("Right  ");
        break;
      case 2:
        tft.print("Down  ");
        break;
      case 3:
        tft.print("LSet   ");
        break;
      case 4:
        tft.print("Up     ");
        break;
      case 5:
        tft.print("Left   ");
        break;
      case 7:
        tft.print("Center");
        break;
    }
    tone(A3, 852, 150);
    delay(100);
    tone(A3, 1457, 100);
    //delay(1000);
    lastNum = dataRecieve.num;
  }

  myRadio.stopListening();
  dataTransmit.id = dataTransmit.id;
  //dataTransmit.temperature = dataTransmit.temperature+0.1;
  Serial.println("Transmit: ");
  Serial.print("Package:");
  Serial.print(dataTransmit.id);
  Serial.print("\n");
  //Serial.println(dataTransmit.temperature);
  //Serial.println(dataTransmit.text);
  Serial.print("\n");
  char inData[300];
  int index = 0;
  while (Serial.available() >= 1) {
    if (index < 500) {
      inData[index] = Serial.read();
      index++;
      inData[index] = '\0';
    //  sprintf(dataTransmit.text, "%s", inData);
    }
  }
  for (int i = 0; i < 8; i++){
   if(pcf.readButton(i) != 1) {
    pcf.write(i, 1);
    direction = i;
    dataTransmit.num = direction;
   }
  }
  myRadio.openWritingPipe(addresses[0]);
  myRadio.write(&dataTransmit, sizeof(dataTransmit));
  myRadio.openReadingPipe(1, addresses[0]);
  myRadio.startListening();
}
